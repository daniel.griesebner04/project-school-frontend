import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'; 

function Header() {
  const location = useLocation().pathname;
  return (
    <div className='mb-3'>
        <ul className="nav nav-tabs">
        <li className="nav-item">
          <Link to="/" className={"nav-link " + (location === '/' ? 'active' : '')}>Students</Link>
        </li>
        <li className="nav-item">
          <Link to="/courses" className={"nav-link " + (location === '/courses' ? 'active' : '')}>Courses</Link>
        </li>
        <li className="nav-item">
          <Link to="/bookings" className={"nav-link " + (location === '/bookings' ? 'active' : '')}>Bookings</Link>
        </li>
        
      </ul>
    </div>
  )
}

export default Header