import React from 'react'
import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import StudentDropdown from './StudentDropdown';
import CourseDropdown from './CourseDropdown';

function BookingAdd( {students, courses, onAdd} ) {

    const [studentId, setStudentId] = useState(Number);
    const [courseId, setCourseId] = useState(Number);
    
const onButtonPress = () => {
    if(studentId != "" && courseId != ""){
        let newBooking = { studentId, courseId}
        onAdd( newBooking )
    }
}

  return (
    <div className='mb-2'>
        <div className='row mb-2'>
        <div className='col-md-6 mb-1'>
                <StudentDropdown className="form-control" students={students} setStudent={setStudentId} defaultValue="" disabled={false}/>
        </div>
        <div className='col-md-6 mb-1'>
                <CourseDropdown className="form-control" courses={courses} setCourse={setCourseId} defaultValue="" disabled={false}/>
        </div>
        
        </div>
        <div className='row'>
            <div className='col-md-4'>
                <button className="btn btn-success" onClick={() => onButtonPress()}>Buchen</button>
            </div>
        </div>
        
        </div>
  )
}

export default BookingAdd