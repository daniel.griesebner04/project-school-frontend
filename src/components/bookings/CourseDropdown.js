import React from 'react'

function CourseDropdown(  {courses, setCourse, defaultValue, disabled, className} ) {
  return (
    <select className={className} aria-label="Course" defaultValue={defaultValue} onChange={(e) => setCourse(e.target.value)} disabled={disabled}>
        <option value="" hidden>Kurs</option>
        {courses?.map(course => (
            <option key={course.id} value={course.id}>{course.courseName}</option>
        ))}
    </select>
  )
}

export default CourseDropdown