import React from 'react'
import { FaTimes } from 'react-icons/fa'
import { BsFillPencilFill } from 'react-icons/bs'
import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'; 
import StudentDropdown from './StudentDropdown';
import CourseDropdown from './CourseDropdown';

function Booking( {booking, onDelete, students, courses, onUpdate} ) {
  const [enabled, setEnabled] = useState(false);

  const [studentId, setStudentId] = useState(booking.student.id);
  const [courseId, setCourseId] = useState(booking.course.id);
  const [course, setCourse] = useState(booking.course);
  const [student, setStudent] = useState(booking.student);

  const updateBooking = async () => {
      setEnabled(!enabled)
      if(enabled){
        let id = booking.id;
        console.log({id, courseId, studentId});
        let response = await onUpdate( {id, courseId, studentId} );
        if(response != undefined){
          booking = response;
          setCourse(booking.course);
          setStudent(booking.student);
        }
      }
      
  }

  return (
    <div className="accordion-item">
      <h2 className="accordion-header" id={booking.id}>
        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={"#collapse"+ booking.id} aria-expanded="false" aria-controls={"collapse"+ booking.id}>
          { course.courseName } - {booking.bookingDate}
        </button>
      </h2>
      <div id={"collapse"+ booking.id} className="accordion-collapse collapse" aria-labelledby={booking.id} data-bs-parent="#bookings">
        <div className="accordion-body">
          <ul className="list-group list-group-flush">
          <li className="list-group-item"><strong>ID:</strong> <input value={booking.id} type="number" disabled/> </li>
          <li className="list-group-item"><strong>Schüler:</strong> <StudentDropdown students={students} setStudent={setStudentId} defaultValue={studentId} disabled={!enabled}/> </li>
          <li className="list-group-item"><strong>Kurs:</strong> <CourseDropdown courses={courses} setCourse={setCourseId} defaultValue={courseId} disabled={!enabled}/> </li>
          <li className="list-group-item"><strong>Buchungsdatum:</strong> <input type="date" value={booking.bookingDate} disabled/> </li>
            <li className="list-group-item"><FaTimes style={{color: 'red', cursor: 'pointer'}} onClick={() => onDelete(booking.id)} /> <BsFillPencilFill style={{color: 'green', cursor: 'pointer'}} onClick={() => updateBooking()} /></li>
          </ul>
        </div>
      </div>
   </div>
  
  )
}

export default Booking