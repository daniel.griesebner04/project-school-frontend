import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

function StudentDropdown( {students, setStudent, defaultValue, disabled , className}) {
  return (
    <select className={className} aria-label="Student" defaultValue={defaultValue} onChange={(e) => setStudent(e.target.value)} disabled={disabled}>
        <option value="" hidden>Schüler</option>
        {students?.map(student => (
            <option key={student.id} value={student.id}>({student.id}) {student.firstname} {student.lastname}</option>
        ))}
    </select>
  )
}   

export default StudentDropdown