import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'; 

const Button = ({text, action, style}) => {
  return (
    <button onClick={action} style={style}>{text}</button>
  )
}

export default Button