import React from 'react'
import { useState, useEffect } from 'react';
import Student from '../students/Student'
import StudentAdd from '../students/StudentAdd';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'bootstrap/dist/js/bootstrap.esm'; 

const StudentView = ( {fetchStudents} ) => {

  const [students, setStudents] = useState([]);

  useEffect(() => {
      const getData = async () => {
        const students = await fetchStudents();
        setStudents(students);
      }
      getData();
  }, []);

  

  

  const onStudentDelete = async (id) => {
      const res = await fetch(`http://localhost:8080/students/${id}`, {
        method: 'DELETE',
      })

      setStudents(students.filter((st) => st.id !== id))
  };


  const onStudentAdd = async (student) => {
    const res = await fetch('http://localhost:8080/students', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(student),
    })
    
    const data = await res.json()
    if(res.status == '201'){
      setStudents([...students, data])
    }
  }

  const onStudentUpdate = async (student) => {
    const res = await fetch('http://localhost:8080/students', {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(student),
    })
    
    const data = await res.json()
    if(res.status == '201'){
      console.log(data)
    }
  }

  return (
    <div className='container'>
      <h2>Students:</h2>

      <StudentAdd onAdd={onStudentAdd}/>
      <div key="accordion" className="accordion" id="students">
        {students?.map(st => (
          <Student key={st.id} student={st} onDelete={onStudentDelete} onUpdate={onStudentUpdate}/>
        ))}
      </div>
    </div>
  )
}

export default StudentView