import React from 'react';
import { useState, useEffect } from 'react';
import Course from '../courses/Course';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import CourseAdd from '../courses/CourseAdd';

function CourseView( {fetchCourses} ) {
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const data = await fetchCourses();
      setCourses(data);
    }
    getData();
}, []);

  

  

  const onCourseDelete = async (id) => {
    const res = await fetch(`http://localhost:8080/courses/${id}`, {
      method: 'DELETE',
    })
    console.log(res)
    setCourses(courses.filter((cr) => cr.id !== id))
  };

  const onCourseAdd = async (course) => {
    const res = await fetch('http://localhost:8080/courses', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(course),
    })
    
    const data = await res.json()
    if(res.status == '201'){
      setCourses([...courses, data])
    }
  }

  const onCourseUpdate = async (course) => {
    const res = await fetch('http://localhost:8080/courses', {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(course),
    })
    
    const data = await res.json()
    console.log(res)
    if(res.status == '201'){
      return undefined;
    }else if(res.status == '422'){
      return data.message;
    }
  }

  return (
    <div className='container'>
      <h2>Courses:</h2>
      <CourseAdd onAdd={onCourseAdd} />
      <div key="accordion" className="accordion" id="courses">
          {courses?.map(cr => (
              <Course key={cr.id} course={cr} onDelete={onCourseDelete} onUpdate={onCourseUpdate}/>  
            ))}
        </div>
      
    </div>
  )
}

export default CourseView