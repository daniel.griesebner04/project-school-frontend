import React from 'react'
import { useState, useEffect } from 'react';
import Booking from '../bookings/Booking';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import BookingAdd from '../bookings/BookingAdd';

function BookingView( {fetchStudents, fetchCourses} ) {

  const [bookings, setBookings] = useState([]);
  const [students, setStudents] = useState([]);
  const [courses, setCourses] = useState([]);

  useEffect(() => {
      const getData = async () => {
        const bookings = await fetchBookings();
        const students = await fetchStudents();
        const courses = await fetchCourses();
        setBookings(bookings);
        setStudents(students);
        setCourses(courses);
      }
      getData();
  }, []);

  const fetchBookings = async () => {
      const result = await fetch("http://localhost:8080/bookings/")
      const data = await result.json()
      return data
  };

  const onBookingDelete = async (id) => {
    const res = await fetch(`http://localhost:8080/bookings/${id}`, {
        method: 'DELETE',
      })

      setBookings(bookings.filter((bk) => bk.id !== id))
  };

  const onBookingAdd = async (booking) => {
    const res = await fetch('http://localhost:8080/bookings', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(booking),
    })
    
    const data = await res.json()
    if(res.status == '201'){
      setBookings([...bookings, data])
    }else{
      alert(data.message)
    }
  }

  const onBookingUpdate = async (booking) => {
    const res = await fetch('http://localhost:8080/bookings', {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(booking),
    })
    
    const data = await res.json()
    if(res.status == '201'){
        return data
    }
  }



  return (
    <div className='container'>
      <h2>Bookings:</h2>
      <BookingAdd students={students} courses={courses} onAdd={onBookingAdd}/>
      <div key="accordion" className="accordion" id="bookings">
            {bookings?.map(bk => (
            <Booking key={bk.id} booking={bk} onDelete={onBookingDelete} students={students} courses={courses} onUpdate={onBookingUpdate}/>  
            ))}
        </div>
  
    </div>
  )
}

export default BookingView