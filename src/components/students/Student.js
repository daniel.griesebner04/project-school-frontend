import React from 'react'
import { FaTimes } from 'react-icons/fa'
import { BsFillPencilFill } from 'react-icons/bs';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { useState } from 'react';

const Student = ( {student, onDelete, onUpdate} ) => {
  
  const [enabled, setEnabled] = useState(false);
  const [firstname, setFirstname] = useState(student.firstname);
  const [lastname, setLastname] = useState(student.lastname);
  const [birthdate, setBirthdate] = useState(student.birthdate);
  const [nummer, setNummer] = useState(student.studentNumber);

  const updateStudent = () => {
    if(enabled){
      if(firstname != "" && lastname != "" && nummer != "" && birthdate != ""){
        student.firstname = firstname;
        student.lastname = lastname;
        student.studentNumber = nummer;
        student.birthdate = birthdate;
        onUpdate( student );
      }
    }
    setEnabled(!enabled)
  }
  

  

  return (
    <div className="accordion-item">
      <h2 className="accordion-header" id={student.id}>
        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={"#collapse"+ student.id} aria-expanded="false" aria-controls={"collapse"+ student.id}>
          {firstname} {lastname}
        </button>
      </h2>
      <div id={"collapse"+ student.id} className="accordion-collapse collapse" aria-labelledby={student.id} data-bs-parent="#students">
        <div className="accordion-body">
          <ul className="list-group list-group-flush">
          <li className="list-group-item"><strong>ID:</strong> <input className='updateField' type="text" value={student.id} disabled/></li>
            <li className="list-group-item"><strong>Vorname:</strong> <input onChange={(e) => setFirstname(e.target.value)} className='updateField' type="text" value={firstname} disabled={!enabled}/></li>
            <li className="list-group-item"><strong>Nachname:</strong> <input onChange={(e) => setLastname(e.target.value)} className='updateField' type="text" value={lastname} disabled={!enabled}/></li>

            <li className="list-group-item"><strong>Geburtsdatum:</strong> <input onChange={(e) => setBirthdate(e.target.value)} className='updateField' type="date" value={birthdate} disabled={!enabled}/></li>
            <li className="list-group-item"><strong>Nummer:</strong> <input onChange={(e) => setNummer(e.target.value)} className='updateField' type="text" value={nummer} disabled={!enabled}/></li>
            <li className="list-group-item"><FaTimes style={{color: 'red', cursor: 'pointer'}} onClick={() => onDelete(student.id)} /> <BsFillPencilFill  style={{color: 'green', cursor: 'pointer'}} onClick={() => updateStudent()} /> </li>
            
          </ul>
        </div>
      </div>
   </div>
  )
}



export default Student