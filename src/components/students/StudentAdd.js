import React from 'react'
import Button from '../Button'
import InputField from '../InputField'
import { useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; 

const StudentAdd = ( {onAdd} ) => {
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [studentNumber, setStudentNumber] = useState('')
  const [birthdate, setDate] = useState('')

  const onButtonPress = () => {
    if(firstname != "" && lastname != "" && studentNumber != "" && birthdate != ""){
      let newStudent = {firstname, lastname, studentNumber, birthdate}
      onAdd( newStudent )
    }
  }
  return (
    <div className='mb-3'>
      <div className='row'>
        <div className='col-md-4 mb-1'>
              <div className="form-floating">
                <input type="text" className="form-control" id="vorname" placeholder="Vorname" onChange={(e) => setFirstname(e.target.value)}/>
                <label htmlFor="vorname">Vorname</label>
              </div>
        </div>
        <div className='col-md-4 mb-1'>
          <div className="form-floating">
                <input type="text" className="form-control" id="nachname" placeholder="Nachname" onChange={(e) => setLastname(e.target.value)}/>
                <label htmlFor="nachname">Nachname</label>
              </div>
        </div>
        <div className='col-md-4 mb-1'>
          <div className="form-floating">
                <input type="text" className="form-control" id="nummer" placeholder="Nachname" onChange={(e) => setStudentNumber(e.target.value)}/>
                <label htmlFor="nummer">Nummer</label>
              </div>
        </div>
      </div>
      <div className='row mb-2'>
          <div className='col-md-4 mb-1'>
            <div className="form-floating">
                  <input type="date" className="form-control" id="geburtsdatum" placeholder="Geburtsdatum" onChange={(e) => setDate(e.target.value)}/>
                  <label htmlFor="geburtsdatum">Geburtsdatum</label>
                </div>
          </div>
        </div>
        
        <button className="btn btn-success" onClick={() => onButtonPress()}>Speichern</button>
    </div>
  )
}

export default StudentAdd