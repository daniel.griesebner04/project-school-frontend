import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'; 

function InputField( {id, plcholder, label, type, action}) {
  return (
    <label htmlFor={id} className='form-label'>
            {label}:<br/>
            <input id={id} classname='form-control' type={type} placeholder={plcholder} onChange={(e) => action(e.target.value)}/>
    </label>
  )
}


export default InputField