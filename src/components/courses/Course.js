import React from 'react'
import { FaTimes } from 'react-icons/fa'
import { BsFillPencilFill } from 'react-icons/bs';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { useState, useEffect } from 'react';

function Course( {course, onDelete, onUpdate}) {
  const [bookingCount, setBookingCount] = useState(0);

  const [enabled, setEnabled] = useState(false);
  const [courseName, setName] = useState(course.courseName);
  const [courseContent, setContent] = useState(course.courseContent);
  const [courseNumber, setNummer] = useState(course.courseNumber);
  const [maxStudents, setMax] = useState(course.maxStudents);
  const [deadLine, setDeadline] = useState(course.deadLine);
  
  useEffect(() => {
    const getData = async () => {
      const data = await fetchCount(course.id);
      setBookingCount(data);
    }
    getData();
}, []);

  const fetchCount = async (id) => {
    const result = await fetch(`http://localhost:8080/courses/${id}/bookingCount`)
    const data = await result.json()
    return data
  }

  const updateCourse = async () => {
    if(enabled){
      if(courseName != "" && courseNumber != "" && courseContent != "" && maxStudents != "" && deadLine != ""){
        let id = course.id;
        let courseUpdate = {id ,courseName, courseNumber, courseContent, maxStudents, deadLine};
        let updated = await onUpdate( courseUpdate )
        
        if(updated == undefined){
          course.courseName = courseName;
          course.courseNumber = courseNumber;
          course.courseContent = courseContent;
          course.maxStudents = maxStudents;
          course.deadLine = deadLine;
        }else{
          alert(updated)
          setName(course.courseName);
          setNummer(course.courseNumber);
          setContent(course.courseContent);
          setMax(course.maxStudents);
          setDeadline(course.deadLine);
        }
      }
    }
    setEnabled(!enabled)
  }

  return (
    <div className="accordion-item">
      <h2 className="accordion-header" id={course.id}>
        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={"#collapse"+ course.id} aria-expanded="false" aria-controls={"collapse"+ course.id}>
          {courseName}
        </button>
      </h2>
      <div id={"collapse"+ course.id} className="accordion-collapse collapse" aria-labelledby={course.id} data-bs-parent="#courses">
        <div className="accordion-body">
          <ul className="list-group list-group-flush">
          <li className="list-group-item"><strong>ID:</strong> <input type="number" value={course.id} disabled/></li>
          <li className="list-group-item"><strong>Name:</strong> <input type="text" onChange={(e) => setName(e.target.value)} value={courseName} disabled={!enabled}/></li>
            <li className="list-group-item"><strong>Inhalt:</strong> <input type="text" onChange={(e) => setContent(e.target.value)} value={courseContent} disabled={!enabled}/></li>
            <li className="list-group-item"><strong>Nummer:</strong> <input type="text" onChange={(e) => setNummer(e.target.value)} value={courseNumber} disabled={!enabled}/></li>
            <li className="list-group-item"><strong>Teilnehmer:</strong> <input type="number" value={bookingCount} disabled/> /<input type="number" onChange={(e) => setMax(e.target.value)} value={maxStudents} disabled={!enabled}/></li>
            <li className="list-group-item"><strong>Deadline:</strong> <input type="date" onChange={(e) => setDeadline(e.target.value)} value={deadLine} disabled={!enabled}/></li>
            <li className="list-group-item"><FaTimes style={{color: 'red', cursor: 'pointer'}} onClick={() => onDelete(course.id)} /> <BsFillPencilFill style={{color: 'green', cursor: 'pointer'}} onClick={() => updateCourse()} /></li>
          </ul>
        </div>
      </div>
   </div>
  )
}

export default Course