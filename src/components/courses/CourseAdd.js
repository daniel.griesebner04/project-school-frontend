import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { useState} from 'react';

function CourseAdd( { onAdd } ) {
    const [courseName, setName] = useState('')
    const [courseNumber, setNumber] = useState('')
    const [courseContent, setContent] = useState('')
    const [maxStudents, setMax] = useState('')
    const [deadLine, setDeadline] = useState('')

    const onButtonPress = () => {
        if(courseName != "" && courseNumber != "" && courseContent != "" && maxStudents != "" && deadLine != ""){
          let newStudent = {courseName, courseNumber, courseContent, maxStudents, deadLine}
          onAdd( newStudent )
        }
      }

  return (
    <div className='mb-3'>
      <div className='row'>
        <div className='col-md-4 mb-1'>
              <div className="form-floating">
                <input type="text" className="form-control" id="name" placeholder="Name" onChange={(e) => setName(e.target.value)}/>
                <label htmlFor="name">Name</label>
              </div>
        </div>
        <div className='col-md-4 mb-1'>
          <div className="form-floating">
                <input type="text" className="form-control" id="inhalt" placeholder="Inhalt" onChange={(e) => setContent(e.target.value)}/>
                <label htmlFor="inhalt">Inhalt</label>
              </div>
        </div>
        <div className='col-md-4 mb-1'>
          <div className="form-floating">
                <input type="text" className="form-control" id="nummer" placeholder="Nachname" onChange={(e) => setNumber(e.target.value)}/>
                <label htmlFor="nummer">Nummer</label>
              </div>
        </div>
      </div>
      <div className='row mb-2'>
        <div className='col-md-4 mb-1'>
            <div className="form-floating">
                  <input type="number" className="form-control" id="maxStudents" placeholder="Max. Teilnehmer" onChange={(e) => setMax(e.target.value)}/>
                  <label htmlFor="maxStudents">Max. Teilnehmer</label>
            </div>
        </div>
          <div className='col-md-4 mb-1'>
                <div className="form-floating">
                  <input type="date" className="form-control" id="deadline" placeholder="Deadline" onChange={(e) => setDeadline(e.target.value)}/>
                  <label htmlFor="deadline">Deadline</label>
                </div>
          </div>
        </div>
        
        <button className="btn btn-success" onClick={() => onButtonPress()}>Speichern</button>
    </div>
  )
}

export default CourseAdd