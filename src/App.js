import StudentView from './components/views/StudentView';
import CourseView from './components/views/CourseView';
import BookingView from './components/views/BookingView';
import Header from './components/Header';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';




function App() {

  
  const fetchStudents = async () => {
    const result = await fetch("http://localhost:8080/students/")
    const data = await result.json()
    return data
  };

  const fetchCourses = async () => {
    const result = await fetch("http://localhost:8080/courses/")
      const data = await result.json()
      return data
  }
  

  return (
    <Router>
      <div className='container mt-5 mb-5'>
        <Header/>
        <Routes>
          <Route
            path='/'
            element={
              <StudentView fetchStudents={fetchStudents}/>
            }
          />
          <Route
            path='/courses'
            element={
              <CourseView fetchCourses={fetchCourses}/>
            }
          />
          <Route
            path='/bookings'
            element={
              <BookingView fetchStudents={fetchStudents} fetchCourses={fetchCourses} />
            }
          />
        </Routes>
      </div> 
    </Router>
  );
}

export default App;
